import org.scalatest.FunSpec

class StringManipulationSpec extends FunSpec {
  def time(f: ⇒ Unit, rep: Int): Double = {
    def averaging(c: Int, total: Long): Long = {
      c match {
        case x if (c == rep) ⇒ total
        case _ ⇒
          averaging(c + 1, total + {
            val s = System.currentTimeMillis
            for (i ← 1 to 100000) (f)
            System.currentTimeMillis - s
          })
      }
    }
    averaging(0, 0) / rep
  }

  ignore("Regex") {
    describe("Regex created once") {
      def WithMatcher(elements: List[String]) = {
        val matcher = """/*(.*?)/*""".r
        elements filterNot (el ⇒ el == null || el.isEmpty) map {
          element ⇒
            val matcher(path) = element
            path
        } mkString ("/")
      }

      println("Regex each.... : %s".format(time(WithMatcher(List("a", "b", "c")), 100)))
    }

    describe("Regex created each time") {
      val matcher = """/*(.*?)/*""".r
      def createPath(elements: List[String]) = {
        elements filterNot (el ⇒ el == null || el.isEmpty) map {
          element ⇒
            val matcher(path) = element
            path
        } mkString ("/")
      }

      println("Regex once.... : %s".format(time(createPath(List("a", "b", "c")), 100)))
    }
}

describe("String formatting") {
    describe("format command") {
      println("Format command : %s".format(time("%s/%s/%s".format("a", "b", "c"), 100)))
    }

    describe("string concatenate") {
      println("Concatenate... : %s".format(time("a" + "/" + "b" + "/" + "c", 100)))
    }

    describe("string interpolation") {
      println("Interpolate... : %s".format(time({
        val a = "a"
        val b = "b"
        val c = "c"
        s"""$a/$b/$c"""
        }, 100)))
    }
  }
}