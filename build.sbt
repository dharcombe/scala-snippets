name := "scala-snippets"

organization := "com.gramercysoftware"

version := "0.0.1"

scalaVersion := "2.10.1"

scalacOptions ++= Seq("-unchecked", "-deprecation")

resolvers ++= Seq(
	"JBoss Repo" at "http://repository.jboss.org/nexus/content/groups/public",
	"Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-slf4j" % "2.0.+",
  "com.typesafe.akka" % "akka-actor" % "2.0.+",
  "com.typesafe.akka" % "akka-zeromq" % "2.0.+"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "1.9.1" % "test",
  "junit" % "junit" % "4.8.1" % "test"
)
